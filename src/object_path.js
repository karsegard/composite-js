import { pipe, map, curry, flip, identity, compose } from './core.js'
import { defaultTo, is_type_object } from './bool.js'

import { either } from './conditional'


export const safe_prop = curry((def, key, obj) => {
  if (obj)
    return (obj.hasOwnProperty(key)) ? obj[key] : def(key);


  return def;
});



export const safe_path = curry((default_value, path, object) => {
  let components = path.split('.').reverse().map(item => safe_prop(default_value, item));

  return compose(...components)(object);

});


