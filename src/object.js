import { pipe, map, curry, flip, identity, compose } from './core.js'
import { defaultTo, is_type_function, is_type_object } from './bool.js'

import { either } from './conditional'


export const assign2 = curry((x, y) => Object.assign({}, x, y))
export const _merge = curry((a, b) => (assign2(a, b)))
export const merge = _merge


export const prop = curry((prop, obj) => obj[prop])


export const keys = o => Object.keys(o)





// String => Object => Object
export const omit_key = curry((_omit, obj) => {
  let o = {};
  Object.keys(obj).map(key => {
    if (key !== _omit) {
      o[key] = obj[key]
    }
  })
  return o;
})



// String => Object => Object
export const omit_keys = curry((_omit, obj) => {
  let o = {};
  Object.keys(obj).map(key => {
    if (_omit.indexOf(key) === -1) {
      o[key] = obj[key]
    }
  })
  return o;
})


export const filter_keys = curry((fn, obj) => {
  let o = {};
  map(
    either(
      fn,
      identity,
      k => o[k] = obj[k]
    ),
    keys(obj)
  )
  return o;
})


export const ensure_object_copy = assign2({})

/*
  String -> String -> Object -> Object
*/
export const as_object_prop = curry((key, value, object) => {
  let o = { ...object }
  o[key] = value;
  return o;
})

//  a -> b -> Object

export const as_prop = curry((key, value) => flip(as_object_prop(key), defaultTo({}), value))


/*
 Spec
  for a given object for which values are function  returns a new object with

  {
    x: fn(a,b),
    y: fn(a,b,c),
  }

  spec(obj,a)
  => {
    x: fn(a,b)(a)
    y: fn(a,b,c)(a)
  }

*/

//Object -> List
export const enlist =(obj) => pipe(
  keys,
  map(x => as_prop(x, obj[x]))
)(obj)

/**
 * 
 * @Signature
 * Function -> ...any -> any
 */
export const run_or_yield = (fn, ...args) => {
  return is_type_function(fn) ? fn(...args) : fn;
}

/**
 * 
 * @Signature 
 * run_or_yield-> Scalar -> any
 *  */
export const safe_prop = curry((def, key, obj) => {
  if (obj)
    return (obj.hasOwnProperty(key)) ? obj[key] : run_or_yield(def, key);


  return run_or_yield(def, key);
});



export const safe_path = curry((default_value, path, object) => {
  let components = path.split('.').reverse().map(item => safe_prop(key => run_or_yield(default_value, key, path), item));

  return compose(...components)(object);

});




/**
 * recursively merge object keys
 * Doesn't merge arrays or values, just the keys
 */
export const deep_merge = (target, source) => {
  let result = merge(target, source);
  if(!is_type_object(source)){
    return null;
  }
  for (const key of Object.keys(source)) {
    if (is_type_object(source[key])) {
      if (is_type_object(safe_prop(undefined,key,target))) {
        result[key] = deep_merge(target[key], source[key])
      } else {
        result[key] = source[key]
      }
    }
  }

  return result
}

/**
 * set or replace value at given path in an object and return new object
 * @param {string} path
 * @param {{}} object
 * @param {any} value
 */
export const as_safe_path = curry((path, object, value) => {
  let components = path.split('.').reverse();
  let { result } = components.reduce((carry, key) => {
    let res = {}

    res[key] = carry.result
    carry.result = res;
    return carry;
  }, { result: value });

  return deep_merge(object, result);

});




export const compare_objects = (o1, o2) => {
  if (!is_type_object(o1) && !is_type_object(o2)) {
    return false
  }
  const keys1 = keys(o1);
  const keys2 = keys(o2);
  if (keys1.length !== keys2.length) {
    return false;
  }
  for (let key of keys1) {
    if (is_type_object(o1[key]) && is_type_object(o2[key])) {
      if(!compare_objects(o1[key], o2[key])){
        return false;
      }
    } else {
      if (o1[key] !== o2[key]) {
        return false;
      }
    }
  }
  return true;
}