


export const curry = (fn,immediate=false) => {
    const innerFn = (N, args) => {
      return function $__curry(...x) {
        let terminate = false;
        if(x.length===0 ){
          if(!immediate){
            x = [undefined];
          }else{
            terminate = true;
          }
        }
        if (N <= x.length  || terminate) {
          return fn(...args, ...x);
        }
        return innerFn(N - x.length, [...args, ...x]);
      };
    };
  
    return innerFn(fn.length, []);
  }
  