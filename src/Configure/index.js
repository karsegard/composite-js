import {compose,is_undefined,is_type_function,
    trace,split,either,defaultTo,curry ,eitherThrow,
    is_array,identity,concat,map} from 'Core'

    import {reduce} from 'array'
import { prop , as_prop, keys,ensure_object_copy, assign2, enlist} from 'object';
import {spec,key} from 'Object'


export const servers_from_string =  split(',')



export const to_array_if_needed = fn =>  either(is_array,fn,identity)

// return the prop value or undefined
export const prop_value_or_undefined = (env_key,env) => compose(defaultTo,either(is_undefined,prop(env_key),x=>undefined))(env)


/**
 *
 * returns env[defaultKey] or defaultValue if not defined
 *
 * DefaultKey => defaultValue => next => env
 * String => Any => Fn => Object => Any
 *  */
export const envThenValue = curry((defaultKey,defaultValue,next,env) => value=> compose(
    next,
    defaultTo(defaultValue),
    prop_value_or_undefined(defaultKey,env)
)(value));


export const justValue = curry((defaultValue,next,env)=> value=> compose(next,defaultTo(defaultValue))(value));

/**
 *
 *  */
export const field = curry((valueConfig,env,key) => compose(as_prop(key),valueConfig(env)));



export const reduceCalling = settings => (acc,item)=> {
    const k = key(item)
    const fn = item[k](k);
    const _v = settings[k]
    return {...acc,...fn(_v)};
}

export const applyValues = (settings) => reduce({},reduceCalling(settings))


/*
Object => Object=> Function => Object => Object
DefaultSettings => Settings=> ApplyFunction => Object => Object
*/
export const applyDefaultSettings = (defaultSettings,settings)=> compose (applyValues(settings),enlist,spec(defaultSettings))


/**
 * Create a function that iterates over defaultSettings
 */
export const makeConfig = defaultSettings=> (env={})=> (settings={}) => {
    return applyDefaultSettings(defaultSettings,settings)(env)
}



// ensure that the passed parameter is a function or throw error

export const ensureFunction = msg=> eitherThrow(is_type_function,msg)
export {identity} from 'Core'




export const make_config = config_keys => (env_vars={}) => (settings={}) => {
    const items = enlist(config_keys);

    items.reduce( (acc,item)=>{
        const _key = key(item);
        const _key_function = item[_key]
        const _given_value = settings[_key]

        return {
            ...acc,
            ..._key_function(_key,env_vars,_given_value)
        }

    },{});

}
