/**
 * Adapted from https://github.com/reduxjs/redux/blob/master/rollup.config.js
 * Copyright (c) 2015-present Dan Abramov
 */

import nodeResolve from '@rollup/plugin-node-resolve';
import babel from 'rollup-plugin-babel';
import commonjs from '@rollup/plugin-commonjs'
import includePaths from 'rollup-plugin-includepaths';
import copy from 'rollup-plugin-copy';
let includePathOptions = {
    include: {},
    paths: ['src'],
    external: [],
    extensions: ['.js', '.json', '.html']
};


const defaultOutputOptions = {
  format: 'cjs',
  indent: false,
  sourcemap: false,
  exports: 'named',
}


const external = ['chalk','is-dom', 'prop-types',
'react','@geekagency/composite-js','@geekagency/gen-classes',
'formik','react-input-mask','react-icons','react-icons/fa',
'react-icons/md','react-icons/libs','react-loading','react-draggable','react-icons/lib'];


const plugins = [

  commonjs({
    include: 'node_modules/**',
  }),
  babel(),
  includePaths(includePathOptions),

];

const defaultFormat = {
  format: 'cjs',
  indent: false,
  sourcemap: false,

  exports: 'named',
}


const defaultConf = {
  external,
  plugins
}


const make_pkg = (input, output,more_plugins=[]) => {
  return Object.assign({}, defaultConf, {
                                          input,
                                          output: Object.assign({}, defaultOutputOptions, { file: output }),
                                          plugins:[...plugins,...more_plugins]
                                        }
  );
}

const copy_package_json = target =>copy({
  targets: [
    { src: 'package.json', dest: target }
  ]
});

export default [
    make_pkg('src/Core/index.js','dist/cjs/Core.js'),
  make_pkg('src/Effect/index.js','dist/cjs/Effect.js'),
  make_pkg('src/chain','dist/cjs/Chain.js'),
  make_pkg('src/deprecated','dist/cjs/Legacy.js'),
  make_pkg('src/Core/index.js','dist/cjs/Core.js'),
  make_pkg('src/Curry/index.js','dist/cjs/Curry.js'),
  make_pkg('src/Minux/index.js','dist/cjs/Minux.js'),
  make_pkg('src/Monad/index.js','dist/cjs/Monad.js'),
  make_pkg('src/Object/index.js','dist/cjs/ObjectUtils.js'),
  make_pkg('src/List/index.js','dist/cjs/List.js'),
  make_pkg('src/Combinator/index.js','dist/cjs/Combinator.js'),
  make_pkg('src/Minux/index.js','dist/cjs/Minux.js'),
  make_pkg('src/ReactUtils/index.js','dist/cjs/ReactUtils.js'),
  make_pkg('src/Validators/index.js','dist/cjs/Validators.js'),
  make_pkg('src/Geometry/index.js','dist/cjs/Geometry.js'),
  make_pkg('src/ReduxUtils/index.js','dist/cjs/ReduxUtils.js'),
  make_pkg('src/StringUtils/index.js','dist/cjs/StringUtils.js'),
  make_pkg('src/StringUtils/index.js','dist/cjs/String.js'),
  make_pkg('src/Configure/index.js','dist/cjs/Configure.js',[ copy_package_json('dist/cjs')]),


  

];
