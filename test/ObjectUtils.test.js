import * as ObjectUtils from 'Object'

import {curry,compose} from 'core'
import {test as _test} from 'string'
import {not} from 'bool'





test('pathes',()=>{
  const o = {
    a:'b',
    c:'d',
    e:{
      f:'g',
      t:'uu',
      hh:{
        zz:'i'
      }
    }
  }

  let res = ObjectUtils.pathes(o);

  expect(res).toEqual({"a": "b", "c": "d", "e.f": "g", "e.hh.zz": "i", "e.t": "uu"});
});


test('pathes 2',()=>{
  const o = {
    a:'b',
    c:'d',
    e:{
      f:'g',
      t:'uu',
      hh:{
        zz:{}
      }
    }
  }

  let res = ObjectUtils.pathes(o);

  expect(res).toEqual( {"a": "b", "c": "d", "e.f": "g", "e.hh.zz": {}, "e.t": "uu"});
});

test('keyval',()=>{
  const o = {
    a:'b',
    c:'d',
  }

  const [key,value] = ObjectUtils.keyval(o);

  expect(key).toEqual('a')
  expect(value).toEqual('b')

});

test('keys',()=>{
  const o = {
    a:'b',
    c:'d',
  }

  expect(ObjectUtils.key(o)).toBe('a');
  expect(ObjectUtils.hasKey('a',o) ) .toBe(true);
  expect(ObjectUtils.hasKey('z',o) ) .toBe(false);



});



test('value',()=>{
  const o = {
    a:'b',
    c:'d',
  }

  expect(ObjectUtils.value(o)).toBe('b');
  expect(ObjectUtils.hasKey('a',o) ) .toBe(true);
  expect(ObjectUtils.hasKey('z',o) ) .toBe(false);



});


test('propThatMatch',()=>{

  const beginWithToolbar = ObjectUtils.propMatch(/^Toolbar/);
  const keyBeginWithToolbar = beginWithToolbar('key');
  expect(
      keyBeginWithToolbar({key:'ToolbarHandleProut'})

  ).toBe(true)
  expect(
    keyBeginWithToolbar({key:'andleProut'})

).toBe(false)


  });


test('rest',()=>{

  let o = {
    toolbarHandleBack: 'aaa',
    toolbarHandleForward: 'bbb',
    handleProp: 'ccc',

  }
  const beginWithToolbar = _test(/^toolbar/);

  expect(
    beginWithToolbar('basdb')
    ).toBe(false)

    expect(
      beginWithToolbar('toolbarHandleForward')
      ).toBe(true)


  const filter = ObjectUtils.filterByKey(beginWithToolbar)
  expect(
    filter(o)
    ).toEqual({"toolbarHandleBack": "aaa", "toolbarHandleForward": "bbb"})



  const filternot = ObjectUtils.filterByKey(compose(not,beginWithToolbar))

  expect(
    filternot(o)
    ).toEqual({"handleProp": "ccc"})


  const easySpread = ObjectUtils.spreadFilterByKey(beginWithToolbar)

  expect(
    easySpread(o)
    ).toEqual([{"toolbarHandleBack": "aaa", "toolbarHandleForward": "bbb"}, {"handleProp": "ccc"}])



  const [{toolbarHandleBack,toolbarHandleForward,handleProp},rest] = ObjectUtils.spreadFilterByKey(beginWithToolbar) (o);

  expect (toolbarHandleBack).toBe('aaa')
  expect (toolbarHandleForward).toBe('bbb')
  expect (rest).toEqual({"handleProp": "ccc"})



});
