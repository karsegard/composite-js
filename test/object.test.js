import { merge, makeMerge, mergeAll, spec, omit_key, omit_keys, filter_keys, safe_prop, safe_path, as_safe_path, compare_objects ,deep_merge} from 'object'
import { spread } from 'array'

import { curry, compose } from '../src/core'

/*
test("makemerge",()=>{

  let merge5 = makeMerge(5);
  let merge6 = makeMerge(6);

  expect(merge5).toBeInstanceOf(Function)
  expect(merge6).toBeInstanceOf(Function)

  let toMerge = [
    {a:'a'},
    {b:'a'},
    {c:'a'},
    {d:'a'},
    {e:'a'},
  ]

  let expected = { a: 'a', b: 'a', c: 'a', d: 'a', e: 'a' }

  let res = spread(merge5)(toMerge)
  expect(res).toBeInstanceOf(Object)
  expect(res).toEqual(expected)

  res = mergeAll(toMerge)
  expect(res).toBeInstanceOf(Object)
  expect(res).toEqual(expected)

})
*/


test("deep_merge",()=> {
  const a = {}
  const b = {a:'',b:{c:'hello'}}

  expect(deep_merge(a,b)).toEqual(b);
  expect(deep_merge(b,a)).toEqual(b);
})

test("deep_merge 2",()=> {
  const a = undefined;
  const b = {a:'',b:{c:'hello'}}

  expect(deep_merge(a,b)).toEqual({"a": "", "b": {"c": "hello"}});
  expect(deep_merge(b,a)).toEqual(null);
})

test("deep_merge 3",()=> {
  const a = {a};
  const b = {a:'',b:{c:'hello'}}

  expect(deep_merge(a,b)).toEqual({"a": "", "b": {"c": "hello"}});
  expect(deep_merge(b,a)).toEqual( {"a": undefined, "b": {"c": "hello"}});
})

test("compare_objects", () => {

  const o1 = {
    "z50": "555",
    "a50": "1.2",
    "res50": "555",
    "rea50": "12",
    "z5": "123",
    "a5": "0.0",
    "res5": "123",
    "rea5": 0,
    "z100": "555",
    "a100": "0.0",
    "res100": "555",
    "rea100": 0
  }

  const o2 = {
    "z50": "555",
    "a50": "1.2",
    "res50": "555",
    "rea50": "12",
    "z5": "123",
    "a5": "0.0",
    "res5": "123",
    "rea5": 0,
    "z100": "555",
    "a100": "0.0",
    "res100": "555",
    "rea100": 0
  }
  const o3 = {
    "z50": "555",
    "a50": "1.2",
    "res50": "555",
    "rea50": "12",
    "z5": "123",
    "a5": "0.0",
    "res5": "123",
    "rea5": 0,
    "z100": "555",
    "a100": "0.0",
    "res100": "51255",
    "rea100": 0
  }
  expect(compare_objects(o1,o2)).toBe(true);
  expect(compare_objects(o1,o3)).toBe(false);
})



test("compare_objects nested", () => {

  const o1 = {
    "z50": "555",
    "a50": "1.2",
    "res50": "555",
    "rea50": "12",
    "z5": "123",
    "a5": "0.0",
    "res5": "123",
    "rea5": 0,
    "z100": "555",
    "a100": "0.0",
    "res100": "555",
    "rea100": 0,
    sub:{
      "z50": "555",
      "a50": "1.2",
      "res50": "555",
      "rea50": "12",
      "z5": "123",
      "a5": "0.0",
      "res5": "123",
      "rea5": 0,
      "z100": "555",
      "a100": "0.0",
      "res100": "555",
      "rea100": 0,
      sub:{
        "z50": "555",
        "a50": "1.2",
        "res50": "555",
        "rea50": "12",
        "z5": "123",
        "a5": "0.0",
        "res5": "123",
        "rea5": 0,
        "z100": "555",
        "a100": "0.0",
        "res100": "555",
        "rea100": 0
      },
    }
  }

  const o2 = {
    "z50": "555",
    "a50": "1.2",
    "res50": "555",
    "rea50": "12",
    "z5": "123",
    "a5": "0.0",
    "res5": "123",
    "rea5": 0,
    "z100": "555",
    "a100": "0.0",
    "res100": "555",
    "rea100": 0,
    sub:{
      "z50": "555",
      "a50": "1.2",
      "res50": "555",
      "rea50": "12",
      "z5": "123",
      "a5": "0.0",
      "res5": "123",
      "rea5": 0,
      "z100": "555",
      sub:{
        "z50": "555",
        "a50": "1.2",
        "res50": "555",
        "rea50": "12",
        "z5": "123",
        "a5": "0.0",
        "res5": "123",
        "rea5": 0,
        "z100": "555",
        "a100": "0.0",
        "res100": "555",
        "rea100": 0
      },
      "a100": "0.0",
      "res100": "555",
      "rea100": 0
    }
  }

  const o3 = {
    "z50": "555",
    "a50": "1.2",
    "res50": "555",
    "rea50": "12",
    "z5": "123",
    "a5": "0.0",
    "res5": "123",
    "rea5": 0,
    "z100": "555",
    "a100": "0.0",
    "res100": "555",
    "rea100": 0,
    sub:{
      "z50": "555",
      "a50": "1.2",
      "res50": "555",
      "rea50": "12",
      "z5": "123",
      "a5": "0.0",
      "res5": "123",
      "rea5": 0,
      "z100": "555",
      sub:{
        "z50": "555",
        "a50": "1.2",
        "res50": "555",
        "rea50": "12",
        "z5": "123",
        "a5": "0.0",
        "res5": "12",
        "rea5": 0,
        "z100": "555",
        "a100": "0.0",
        "res100": "555",
        "rea100": 0
      },
      "a100": "0.0",
      "res100": "555",
      "rea100": 0
    }
  }


  expect(compare_objects(o1,o2)).toBe(true);
  expect(compare_objects(o1,o3)).toBe(false);
})


test("safe_prop", () => {

  const obj = {};
  const obj2 = { key: 'a' };

  let a;
  const safe_array = safe_prop([]);

  expect(safe_array('key', obj)).toEqual([]);



  expect(safe_array('key', a)).toEqual([]);
  expect(safe_array('key', obj2)).toEqual('a');


  expect(safe_prop(null, 'key', a)).toEqual(null);
  expect(safe_prop('abcd', 'key', a)).toEqual('abcd');


})


test("safe_prop run_or_yield", () => {

  const obj = {};
  const obj2 = { key: 'a' };

  let a;


  const __def = key => {

    if (key == 'key') {
      return [];
    }

    return 'oups'
  }
  const safe_array = safe_prop(__def);



  expect(safe_array('key', obj)).toEqual([]);



  expect(safe_array('key', a)).toEqual([]);
  expect(safe_array('key', obj2)).toEqual('a');


  expect(safe_prop(null, 'key', a)).toEqual(null);
  expect(safe_prop('abcd', 'key', a)).toEqual('abcd');


})

test("safe_prop_composition", () => {

  const obja = {
    a: {
      b: 'c'
    }
  }

  let objb
  const get_prop = safe_prop(null);

  const get_sub = compose(get_prop('b'), get_prop('a'));

  expect(get_sub(obja)).toEqual('c')


  expect(get_sub(objb)).toEqual(null);



})


test("safe_path", () => {

  const obja = {
    a: {
      b: 'c'
    }
  }

  let objb

  const get = safe_path(null);

  expect(get('a.b', obja)).toEqual('c')
  expect(get('a.b.c', obja)).toEqual(null)
  expect(get('a.b', objb)).toEqual(null)



  const get_array = safe_path([]);

  expect(get_array('a.b', obja)).toEqual('c')
  expect(get_array('a.b.c', obja)).toEqual([])
  expect(get_array('a.b', objb)).toEqual([])





})



test("safe_path with function", () => {

  const obja = {
    a: {
      b: 'c'
    }
  }

  let objb

  const get = safe_path(key => {
    return null;
  });

  expect(get('a.b', obja)).toEqual('c')
  expect(get('a.b.c', obja)).toEqual(null)
  expect(get('a.b', objb)).toEqual(null)



  const get_array = safe_path(key => []);

  expect(get_array('a.b', obja)).toEqual('c')
  expect(get_array('a.b.c', obja)).toEqual([])
  expect(get_array('a.b', objb)).toEqual([])



  const get_cond = safe_path((key, path) => {
    switch (key) {
      case 'c':
        return 'zz';
      case 'b':
        return 'xx';
    }
    return 'oups'
  });

  expect(get_cond('a.b', obja)).toEqual('c')
  expect(get_cond('a.b.c', obja)).toEqual('zz')
  expect(get_cond('a.b', objb)).toEqual('xx')




})


test("as_safe_path", () => {

  let obj = {}
  let result = as_safe_path('test.hello', obj, 'hello world');
  expect(result).toEqual({ "test": { "hello": "hello world" } });


  let existing = {
    name: 'test',
    groups: {
      class: 'test'
    }
  }

  result = as_safe_path('groups.path', existing, 'hello world');


  expect(result).toEqual({ "groups": { "class": "test", "path": "hello world" }, "name": "test" });




})



test("as_safe_path should not erase existing keys", () => {


  let o1 = {
    my: {

      prop: 'hello world'

    }
  }

  let result1 = as_safe_path('my.prop2', o1, 'hello world again');

  expect(result1).toEqual({ "my": { "prop": "hello world", "prop2": "hello world again" } });

  let o = {
    my: {
      deep: {
        prop: 'hello world'
      }
    }
  }

  let result = as_safe_path('my.deep.prop2', o, 'hello world again');

  expect(result).toEqual({ "my": { "deep": { "prop": "hello world", "prop2": "hello world again" } } });

  let o3 = {
    my: {
      very: {
        deep: {
          prop: 'hello world'
        }
      }
    }
  }

  result = as_safe_path('my.very.deep.prop2', o3, 'hello world again');

  expect(result).toEqual({ "my": { "very": { "deep": { "prop": "hello world", "prop2": "hello world again" } } } });
})


test("as_safe_path existing target should be replaced by source", () => {

  let existing = {
    name: 'test',
    groups: {
      class: 'test'
    }
  }

  let result = as_safe_path('groups.class', existing, 'hello world');


  expect(result).toEqual({ "groups": { "class": "hello world" }, "name": "test" });




})




test("as_safe_path not existing target", () => {

  let existing = {

  }

  let result = as_safe_path('mesure.range.from', existing, 'hello world');


  expect(result).toEqual({ "mesure": { "range": { "from": "hello world" } } });




})


test("as_safe_path should accept a non path argument", () => {

  let existing = {
    name: 'test',
    groups: {
      class: 'test'
    }
  }

  let result = as_safe_path('name', existing, 'hello world');


  expect(result).toEqual({ "groups": { "class": "test" }, "name": "hello world" });




})

test("omit_key", () => {
  let mykesy = {
    a: 0,
    b: 1,
    c: 3
  }

  expect(omit_key('a', mykesy)).toEqual({ b: 1, c: 3 })


})

test("omit_keys", () => {
  let mykesy = {
    a: 0,
    b: 1,
    c: 3
  }

  expect(omit_keys(['a', 'b'], mykesy)).toEqual({ c: 3 })


})


test("filter_keys", () => {
  let obj = {
    id: 'a',
    crap: 'nasdg',
    valuable: [],
    anothercrap: {}
  }


  let keeps = keys => key => {
    return keys.indexOf(key) !== -1
  }


  let omit = keys => key => {
    return keys.indexOf(key) === -1;
  }


  let keepfilter = keeps(['id', 'valuable']);
  let omitfilter = omit(['id', 'valuable']);

  expect(filter_keys(keepfilter, obj)).toEqual({ "id": "a", "valuable": [] })


  expect(filter_keys(omitfilter, obj)).toEqual({ "anothercrap": {}, "crap": "nasdg" })



})
/*
test("spec",()=>{


  const tospec = {
      add: curry((x,y)=> x+y),
      times: curry((x,y)=> x*y),
  }

  const test = spec(tospec);
  const result = test(12);

  expect(
    result.add(1)
  ).toBe(13);

  expect(
    result.times(12)
  ).toBe(144)


})
*/