import {curry} from '../src/Curry'



test('curry with no arg', () => {

  //  console.log(supertrace('fn')('lol')('ahi'))
    let fn = curry((x,y)=>{
      return x + y;
    })
  

    console.log('result',fn(1)(1));  
  });




test('curry with no arg', () => {

  //  console.log(supertrace('fn')('lol')('ahi'))
    let fn = curry((x,y)=>{
      return x + y;
    })
  

    console.log('result',fn(1)());  
  });




test('curry instant terminate if called with no arg', () => {

  //  console.log(supertrace('fn')('lol')('ahi'))
    let fn = curry(function(x,y,z){
      console.log(x,y,z)
      console.log(arguments.length)
    },true)
  

    console.log('result',fn(1)());  
    console.log('result',fn(1,2)());  
    console.log('result',fn(1,2)(3));  
  });
